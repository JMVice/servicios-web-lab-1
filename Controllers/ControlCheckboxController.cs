﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace lab1API.Controllers
{
    public class ControlCheckboxController : ApiController
    {
        // Objeto random para generar numeros random.
        Random rnd = new Random();

        // GET api/ControlCheckbox/tamanioArreglo/NumeroMaximo
        public int[] Get(string token1, string token2)
        {
            try
            {
                // Se guardan los valores de tamaño de arreglo y numero máximo.
                int tamanioArreglo = Int32.Parse(token1);
                int numeroMaximo = Int32.Parse(token2);

                // Se crea un arreglo vacio con el tamaño establecido.
                int[] arreglo = new int[tamanioArreglo];

                //Se rellena el arreglo con el metodo que genera numeros aleatorios.
                for (int i = 0; i < tamanioArreglo; i++)
                {
                    arreglo[i] = generarNumeroMaximoAleatorio(numeroMaximo);
                }

                // Se retorna el arreglo.
                return arreglo;
            }
            catch (Exception)
            {
                // Se retorna 0 si hay un error.
                int[] i = new int[] { 0 };
                return i;
            }
        }

        // Metodo que genera un número aleatorio segun el número máximo dado.
        private int generarNumeroMaximoAleatorio(int numeroMaximo)
        {
            return rnd.Next(numeroMaximo);
        }

    }
}
