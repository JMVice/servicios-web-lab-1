﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace lab1API.Controllers
{
    public class SalarioController : ApiController
    {
        // GET api/salario/salarioPorHoras/diasTrabajados/deducciones
        public float[] Get(int token1, int token2, int token3)
        {

            // Se hacen los calculos respectivos.
            float salarioBruto_resultado = (token1 * 7) * token2;
            float deducciones_resultado = (salarioBruto_resultado / 100) * token3;
            float salarioNeto_resultado = salarioBruto_resultado - deducciones_resultado;

            // Se envian los resultados.
            return new float[] { salarioBruto_resultado, deducciones_resultado, salarioNeto_resultado };
        }
    }
}
