﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace lab1API.Controllers
{
    public class ControlWhileController : ApiController
    {
        // GET api/ControlWhile/tipoCliente/subTotal
        public string[] Get(string token1, float token2)
        {

            // Variables que se hacen equivalentes a los valores dados en la API.
            string tipoCliente = token1.ToUpper();
            float subtotal = token2;

            // Variables de calculo.
            float porcentajeDeDescuento = 0;
            float montoDeDescuento = 0;
            float total = 0;

            // Se esblece el descuento segun el tipo de cliente.
            switch (tipoCliente)
            {
                case "A":
                    porcentajeDeDescuento = 25;
                    break;
                case "B":
                    porcentajeDeDescuento = 20;
                    break;
                case "C":
                    porcentajeDeDescuento = 15;
                    break;
                case "D":
                    porcentajeDeDescuento = 10;
                    break;
                case "E":
                    porcentajeDeDescuento = 5;
                    break;
                default:
                    break;
            }

            // Se calcula y establece el monto de descuento.
            montoDeDescuento = (porcentajeDeDescuento / 100) * subtotal;

            // Establece el total con descuento en la caja de texto de total.
            total = subtotal - montoDeDescuento;

            // Se crea un arreglo de string para mostrar los valores finales.
            string[] resultadoFinal = new string[] { porcentajeDeDescuento + "%"
                , "" + montoDeDescuento
                , "" + total};

            return resultadoFinal;
        }
    }
}
