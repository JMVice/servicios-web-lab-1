﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace lab1API.Controllers
{
    public class ManejoDeHilerasRapidasController : ApiController
    {
        // GET api/ManejoDeHilerasRapidas/hilera/
        public string[] Get(string token1)
        {
            string hilera = token1;

            // Se optienen los valores de cada hilera por el uso de metodos que calculan los resultados.
            int totalDeVocales = generarTotalDeVocales(hilera);
            int totalDeConsontanes = generarTotalDeConsonantes(hilera);
            string hileraAlreves = generarHileraAlreves(hilera);
            string[] hileraEnDos = generarHileraEnDos(hilera);

            // Arreglo que guarda todos los resultados.
            string[] resultadoFinal = new string[]
            {
                "" + totalDeVocales
                ,"" + totalDeConsontanes
                ,hileraAlreves
                ,hileraEnDos[0]
                ,hileraEnDos[1]
            };

            return resultadoFinal;
        }

        private int generarTotalDeVocales(string hilera)
        {
            string[] vocales = new string[] { "a", "e", "i", "o", "u" };

            int cantidadDeVocales = 0;

            for (int i = 0; i < hilera.Length; i++)
            {
                for (int j = 0; j < vocales.Length; j++)
                {
                    if (hilera[i].ToString().ToLower() == vocales[j])
                    {
                        cantidadDeVocales++;
                        break;
                    }
                }
            }

            return cantidadDeVocales;
        }

        private int generarTotalDeConsonantes(string hilera)
        {
            string[] consonantes = new string[] { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "y", "z" };

            int cantidadDeConsonantes = 0;

            for (int i = 0; i < hilera.Length; i++)
            {
                for (int j = 0; j < consonantes.Length; j++)
                {
                    if (hilera[i].ToString().ToLower() == consonantes[j])
                    {
                        cantidadDeConsonantes++;
                        break;
                    }
                }
            }
            return cantidadDeConsonantes;
        }

        private string[] generarHileraEnDos(string hilera)
        {
            // Se hara uso de éste algoritmo que parte strings en dos.
            // Fuente: https://www.codeproject.com/Questions/1081689/How-do-i-split-my-my-string-in-equal-half-and-stor
            var MyString = hilera;
            var first = MyString.Substring(0, (int)(MyString.Length / 2));
            var last = MyString.Substring((int)(MyString.Length / 2), (int)(MyString.Length / 2));

            // Se retorna el resultado dentro de un arreglo.
            string[] arreglo = new String[] { first, last };

            return arreglo;
        }

        private string generarHileraAlreves(string hilera)
        {
            // Se hace uso del siguiente algoritmo para volvear cualquier string.
            // Fuente: https://stackoverflow.com/questions/228038/best-way-to-reverse-a-string

            string s = hilera;
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);

            return new string(charArray);
        }
    }
}
