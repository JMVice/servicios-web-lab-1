﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lab1API.Controllers
{
    public class HomeController : Controller
    {
        // Página de ejercicio Radiobutton. /home/Radiobutton/
        public ActionResult Radiobutton()
        {
            return View();
        }

        // Página de ejercicio ProgramacionOrientadaAObjetos. /home/ProgramacionOrientadaAObjetos/
        public ActionResult ProgramacionOrientadaAObjetos()
        {
            return View();
        }

        // Página de ejercicio Manejo de Hileras rapidas. /home/ManejoDeHilerasRapidas/
        public ActionResult ManejoDeHilerasRapidas()
        {
            return View();
        }

        // Página de ejercicio Manejo de Hileras. /home/ManejoDeHileras/
        public ActionResult ManejoDeHileras()
        {
            return View();
        }

        // Página de ejercicio Control While. /home/controlwhile/
        public ActionResult ControlWhile()
        {
            return View();
        }

        // Página de ejercicio Control Checkbox. /home/conversiones/
        public ActionResult ControlCheckbox()
        {
            return View();
        }

        // Página de ejercicio conversiones. /home/conversiones/
        public ActionResult Conversiones()
        {
            return View();
        }

        // Página de ejercicio salario. /home/salario/
        public ActionResult Salario()
        {
            return View();
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
