﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Globalization;

namespace lab1API.Controllers
{
    public class ConversionesController : ApiController
    {
        // GET api/conversiones/numeroAConvertir/decimalDelNumeroAConvertir
        public string[] Get(string token1, string token2)
        {
            try
            {
                // Se comprueba si el número es menor o igual a 32000 con un metodo.
                // También se comprueba si el token2 el cual corresponde al número decimal no sea mayor a 2.
                // También se comprueba si el token2 el cual corresponde al número decimal no sea doble cero.
                if (esElNumeroMenorOIgualA32000(token1) &&
                    token2.Length == 2 &&
                    losDecimalesNoSonDobleCero(token2))
                {

                    // Se adjuntan los resultados en un arreglo de String. Los valores se muestran en el mismo orden
                    // que en el formulario grafico.
                    string[] resultadoFinal = new string[] {token1
                ,token1
                ,token1
                ,token1 + "." + token2 + "f"
                ,token1 + "." + token2};
                    return resultadoFinal;
                }
                else
                {
                    // Si hay un error, se muestra el siguiente texto.
                    string[] resultadoFinal = new string[] { "Error Por favor revise que esta ingresando en la API." +
                    " El numero debe ser menor a 32000 y llevar al menos dos decimales que no sean ambos iguales a 0" };
                    return resultadoFinal;
                }
            }
            catch (Exception)
            {
                // Si hay un error, se muestra el siguiente texto.
                string[] resultadoFinal = new string[] { "Error Por favor revise que esta ingresando en la API." +
                    " El numero debe ser menor a 32000 y llevar al menos dos decimales que no sean ambos iguales a 0" };
                return resultadoFinal;
            }
           
        }

        // Metodo que valida si los decimales no son doble cero.
        private bool losDecimalesNoSonDobleCero(string token2)
        {
            return !token2.Equals("00");
        }

        // Metodo que valida si el número digitado es menor o igual a 32000
        private bool esElNumeroMenorOIgualA32000(string numero)
        {
            double numeroTemporal = double.Parse(numero);
            if (numeroTemporal <= 32000)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // Este fue código de prueba para intentar hacer funcionar la API con conversiones reales.

        // GET api/conversiones/numeroAConvertir
        public string[] Get(string token1)
        {
            if (tieneElNumeroSoloDosDecimalesDiferentesACero(token1) &&
                esElNumeroMenorOIgualA32000(token1))
            {
                // Por hacer: Short, Int, Long, Float, Double.
                string numeroTokenConComa = token1;
                string numeroTokenConPunto = remplazarComaPorPunto(numeroTokenConComa);
                string numeroTokenSinDecimales = borrarUltimosTresNumerosDelNumero(numeroTokenConComa);

                // Converción a Double.
                double numeroDouble = Convert.ToDouble(numeroTokenConPunto);

                // Conversion a Int.
                int numeroInt = Int32.Parse(numeroTokenSinDecimales);

                // Conversion a Short.
                short numeroShort = Int16.Parse(numeroTokenSinDecimales);

                // Conversion a Float.
                float numeroFloat = float.Parse(numeroTokenConPunto, CultureInfo.InvariantCulture.NumberFormat);

                // Conversion a long.
                long numeroLong = Int64.Parse(numeroTokenSinDecimales);

                string[] resultadoFinal = new string[] { "Numero double: " + numeroDouble
                    , "Numero short: " +  numeroShort
                ,"Numero Int: " + numeroInt
                ,"Numero float: " + numeroFloat
                ,"Numero long" + numeroLong};
                return resultadoFinal;
            }
            else
            {
                string[] resultadoFinal = new string[] { "Error Por favor revise que esta ingresando en la API." +
                    " El numero debe ser menor a 32000 y llevar al menos dos decimales que no sean ambos iguales a 0" };
                return resultadoFinal;
            }
        }

        private string remplazarComaPorPunto(string token1)
        {
            StringBuilder sb = new StringBuilder(token1);
            sb.Replace(",", ".");
            return sb.ToString();
        }

        private bool tieneElNumeroSoloDosDecimalesDiferentesACero(string numero)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder(numero);

                while (stringBuilder[0] != ',')
                {
                    stringBuilder.Remove(0, 1);
                }
                stringBuilder.Remove(0, 1);

                if (stringBuilder.ToString().Length > 2 &&
                    stringBuilder.ToString().Length < 0 &&
                    !stringBuilder.ToString().Equals("00"))
                {
                    return true;
                }
                else
                {
                    Console.WriteLine("tieneElNumeroSoloDosDecimalesDiferentesACero: false");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error en tieneElNumeroSoloDosDecimalesDiferentesACero");
                Console.Error.WriteLine("Error en tieneElNumeroSoloDosDecimalesDiferentesACero");
                Console.WriteLine(ex);
                return false;
            }

        }

        private string borrarUltimosTresNumerosDelNumero(string numero)
        {
            StringBuilder stringBuilder = new StringBuilder(numero);

            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            stringBuilder.Remove(stringBuilder.Length - 1, 1);

            return stringBuilder.ToString();
        }
    }
}
