﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace lab1API.Controllers
{
    public class ManejoDeHilerasController : ApiController
    {
        // GET api/manejoDeHileras/hilera1/hilera2
        public string[] Get(string token1, string token2)
        {
            string hilera_1 = token1;
            string hilera_2 = token2;

            // Se optienen los valores de cada hilera por el uso de metodos que calculan los resultados.
            string hileraMasLarga = obtenerHileraMasLarga(hilera_1, hilera_2);
            string hileraConMasVocales = obtenerHileraConMasVocales(hilera_1, hilera_2);
            string hileraConMasConsonantes = obtenerHileraConMasConsonantes(hilera_1, hilera_2);
            string hileraConMasCaracteresNoNumericos = obtenerhileraConMasCaracteresNoAlfanumericos(hilera_1, hilera_2);


            // String que guarda todos los resultados.
            string[] resultadoFinal = new string[]
            {
                hileraMasLarga
                ,hileraConMasVocales
                ,hileraConMasConsonantes
                ,hileraConMasCaracteresNoNumericos
            };

            return resultadoFinal;
        }

        // Metodo que retorna la hilera más larga.
        private string obtenerHileraMasLarga(string hilera_1, string hilera_2)
        {
            int longitudHilera1 = hilera_1.Length;
            int longitudHilera2 = hilera_2.Length;

            if (longitudHilera1 > longitudHilera2)
            {

                return hilera_1;
            }
            else
            {
                return hilera_2;
            }
        }

        // Metodo que retorna la hilera con más vocales.
        private string obtenerHileraConMasVocales(string hilera_1, string hilera_2)
        {
            string[] vocales = new string[] { "a", "e", "i", "o", "u" };

            int cantidadVocales_hilera1 = 0;
            int cantidadVocales_hilera2 = 0;

            for (int i = 0; i < hilera_1.Length; i++)
            {
                for (int j = 0; j < vocales.Length; j++)
                {
                    if (hilera_1[i].ToString().ToLower() == vocales[j])
                    {
                        cantidadVocales_hilera1++;
                        break;
                    }
                }
            }

            for (int i = 0; i < hilera_2.Length; i++)
            {
                for (int j = 0; j < vocales.Length; j++)
                {
                    if (hilera_2[i].ToString().ToLower() == vocales[j])
                    {
                        cantidadVocales_hilera2++;
                        break;
                    }
                }
            }

            if (cantidadVocales_hilera1 > cantidadVocales_hilera2)
            {
                return hilera_1;
            }
            else
            {
                return hilera_2;
            }
        }

        // Metodo que retorna la hilera con más consonantes.
        private string obtenerHileraConMasConsonantes(string hilera_1, string hilera_2)
        {
            string[] consonantes = new string[] { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "y", "z" };

            int cantidadConsonantes_hilera1 = 0;
            int cantidadConsonantes_hilera2 = 0;

            for (int i = 0; i < hilera_1.Length; i++)
            {
                for (int j = 0; j < consonantes.Length; j++)
                {
                    if (hilera_1[i].ToString().ToLower() == consonantes[j])
                    {
                        cantidadConsonantes_hilera1++;
                        break;
                    }
                }
            }

            for (int i = 0; i < hilera_2.Length; i++)
            {
                for (int j = 0; j < consonantes.Length; j++)
                {
                    if (hilera_2[i].ToString().ToLower() == consonantes[j])
                    {
                        cantidadConsonantes_hilera2++;
                        break;
                    }
                }
            }

            if (cantidadConsonantes_hilera1 > cantidadConsonantes_hilera2)
            {
                return hilera_1;
            }
            else
            {
                return hilera_2;
            }
        }

        // Metodo que retorna la hilera con más caracteres no alfanumericos
        private string obtenerhileraConMasCaracteresNoAlfanumericos(string hilera_1, string hilera_2)
        {
            string[] noAlphanumericos = new string[] { "!", "@", "#", "&", "(", ")", "–", "[", "{", "}", "]", ":", ";", "'", "?", "/", "*", "~", "$", "^", "+", "=", "<", ">", "," };

            int cantidad_hilera1 = 0;
            int cantidad_hilera2 = 0;

            for (int i = 0; i < hilera_1.Length; i++)
            {
                for (int j = 0; j < noAlphanumericos.Length; j++)
                {
                    if (hilera_1[i].ToString().ToLower() == noAlphanumericos[j])
                    {
                        cantidad_hilera1++;
                        break;
                    }
                }
            }

            for (int i = 0; i < hilera_2.Length; i++)
            {
                for (int j = 0; j < noAlphanumericos.Length; j++)
                {
                    if (hilera_2[i].ToString().ToLower() == noAlphanumericos[j])
                    {
                        cantidad_hilera2++;
                        break;
                    }
                }
            }

            if (cantidad_hilera1 > cantidad_hilera2)
            {
                return hilera_1;
            }
            else
            {
                return hilera_2;
            }
        }
    }
}
