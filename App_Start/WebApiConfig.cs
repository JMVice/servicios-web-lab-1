﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace lab1API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            // Web API para recibir 3 tokens.
            config.Routes.MapHttpRoute(
                name: "API3Tokens",
                routeTemplate: "api/{controller}/{token1}/{token2}/{token3}"
            );

            // Web API para recibir 2 tokens.
            config.Routes.MapHttpRoute(
                name: "API2Tokens",
                routeTemplate: "api/{controller}/{token1}/{token2}"
            );

            // Web API para recibir 1 tokens.
            config.Routes.MapHttpRoute(
                name: "API1Token",
                routeTemplate: "api/{controller}/{token1}"
            );
        }
    }
}
