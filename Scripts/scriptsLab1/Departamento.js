﻿// Clase departamento.
class Departamento {
    constructor(codigo, nombre, presupuesto) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.presupuesto = presupuesto;
    }
}