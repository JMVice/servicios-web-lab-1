﻿// Clase empleado.
class Empleado {
    constructor(nombre, apellidos, departamento, dni) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.departamento = departamento;
        this.dni = dni;
    }
}