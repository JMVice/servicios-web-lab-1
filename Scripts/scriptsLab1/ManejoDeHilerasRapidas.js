﻿// Variable correspondiente a la hilera digitada por el usuario.
const hilera_id = document.getElementById("hilera_id");

// Variables correspondientes a las cajas de texto de resultados.
const totalVocales_id = document.getElementById("totalVocales_id");
const totalConsonantes_id = document.getElementById("totalConsonantes_id");
const hileraAlReves_id = document.getElementById("hileraAlReves_id");
const hileraEnDosParte1_id = document.getElementById("hileraEnDosParte1_id");
const hileraEnDosParte2_id = document.getElementById("hileraEnDosParte2_id");

// Variable correspondiente al boton de ejecutar
const boton_ejecutar = document.getElementById("boton_ejecutar");

// Control de ejecución del botón.
boton_ejecutar.addEventListener('click', e => {

    // Establecemos el número total de vocales que tiene la hilera.
    totalVocales_id.value = generarTotalDeVocales(hilera_id.value);

    // Establecemos el número total de consonantes que tiene la hilera.
    totalConsonantes_id.value = generarTotalDeConsonantes(hilera_id.value);

    // Establecemos el valor de la hilera pero alreves.
    hileraAlReves_id.value = generarHileraAlreves(hilera_id.value);

    // Obtenemos la hilera exactamente partida a la mitad dentro de un arreglo.
    let arregloHileraEnDos = generarHileraEnDosDentroDeArreglo(hilera_id.value);

    // Establecemos el arreglo dentro de las cajas de texto respectivas.
    hileraEnDosParte1_id.value = arregloHileraEnDos[0];
    hileraEnDosParte2_id.value = arregloHileraEnDos[1];
});

// Funcion que genera el numero total de vocales en la hilera.
function generarTotalDeVocales(hilera) {
    let vocales = ['a', 'e', 'i', 'o', 'u'];

    let cantidadVocales = 0;

    // Buscamos el número de vocales dentro de la hilera con un
    // bucle for al igual que el ejercicio de Manejo de Hileras.
    for (let i = 0; i < hilera.length; i++) {
        for (let j = 0; j < vocales.length; j++) {
            if (hilera[i].toLowerCase() == vocales[j]) {
                cantidadVocales++;
                break;
            }
        }
    }

    // Retornamos el número de vocales.
    return cantidadVocales;
}

// Funcion que genera el numero total de consonates en la hilera.
function generarTotalDeConsonantes(hilera) {
    let consonantes = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'y', 'z'];

    let cantidadConsonantes = 0;

    for (let i = 0; i < hilera.length; i++) {
        for (let j = 0; j < consonantes.length; j++) {
            if (hilera[i].toLowerCase() == consonantes[j]) {
                cantidadConsonantes++;
                break;
            }
        }
    }

    return cantidadConsonantes;
}

// Funcion que retorna la hilera pero al reves.
function generarHileraAlreves(hilera) {
    // Se hace uso de éste algoritmo que automaticamente voltea cualquier
    // string alreves.
    // Fuente: https://stackoverflow.com/questions/958908/how-do-you-reverse-a-string-in-place-in-javascript
    return hilera.split("").reverse().join("");
}

// Funcion que parte la hilera en dos y retorna los resultados en un
// arreglo. Se utiliza un algoritmo que hace esto por nosotros.
// Fuente: https://forums.htmlhelp.com/index.php?showtopic=43919
function generarHileraEnDosDentroDeArreglo(hilera) {
    let hilera_temporal = hilera;

    // Algoritmo que corta la hilera en dos. Especificamente en
    // dos variables llamadas s1 y s2.
    var middle = Math.ceil(hilera_temporal.length / 2);
    var s1 = hilera_temporal.slice(0, middle);
    var s2 = hilera_temporal.slice(middle);

    var arreglo = [ s1, s2 ];
    return arreglo;
}