﻿// Declaración de variable para el número que el usuario digite.
const numeroParaConvertir_id = document.getElementById('numeroParaConvertir_id');

// Declaración de variables de las cajas de texto de los resultados.
const shortResultado_id = document.getElementById('shortResultado_id');
const intResultado_id = document.getElementById('intResultado_id');
const longResultado_id = document.getElementById('longResultado_id');
const floatResultado_id = document.getElementById('floatResultado_id');
const doubleResultado_id = document.getElementById('doubleResultado_id');

// Declaración de variables de los botones de ver resultados y limpiar cajas de texto.
const boton_verResultados = document.getElementById('boton_verResultados');
const boton_limpiar = document.getElementById('boton_limpiar');

// Boton que hace las converciones de números y los muestra en pantalla.
boton_verResultados.addEventListener('click', e => {
    let numeroParaConvertir = numeroParaConvertir_id.value;


    // Se hacen las validaciones correspondientes:
    // Numero tiene dos decimales.
    // Numero es menor o igual a 32000 ya que no puede ser mayor que un numero short.
    if (elNumeroContienePuntoYDosDecimales(numeroParaConvertir) &&
        elNumeroEsMenorOIgualA32000(numeroParaConvertir)) {

        let numeroConvertido_float = parseFloat(numeroParaConvertir);
        let numeroConvertido_int = parseInt(numeroParaConvertir);

        floatResultado_id.value = numeroConvertido_float + 'f';
        intResultado_id.value = numeroConvertido_int;
        shortResultado_id.value = numeroConvertido_int;
        longResultado_id.value = numeroConvertido_int;
        doubleResultado_id.value = numeroConvertido_float;
    }
});

// Para marcar los decimales del número a convertir, se hará la validación con punto
// y no con coma. Tambien se evalua si el número ingresado contiene punto.
function elNumeroContienePuntoYDosDecimales(numeroParaConvertir) {

    // El número no contiene coma?
    if (!numeroParaConvertir.includes(',')) {

        // El número contiene punto?
        if (numeroParaConvertir.includes('.')) {

            // El número contiene como mínimo y máximo 2 decimales?
            let numeroParaConvertir_temporal = numeroParaConvertir;
            while (numeroParaConvertir_temporal.charAt(0) != '.') {
                numeroParaConvertir_temporal = numeroParaConvertir_temporal.substring(1);
            }
            if (numeroParaConvertir_temporal.length == 3) {
                // Si llegamos aquí, todo está correcto.
                return true;
            } else {
                alert("El número sólo puede tener dos decimales.");
                return false;
            }
        } else {
            alert("El número decimal debe incluir dos números decimales separados por coma.");
            return false;
        }
    } else {
        alert(`El número decimal no puede contener coma ","`);
        return false;
    }
}

// Evalua si el número no es mayor a 32000 ya que es muy cercano al valor máximo de
// el tipo de dato: Short.
function elNumeroEsMenorOIgualA32000(numeroParaConvertir) {
    // Convertimos el número a Integer.
    let numeroParaConvertir_temporal = parseInt(numeroParaConvertir);

    // Evaluamos si el número es menor o igual a 32000
    if (numeroParaConvertir_temporal <= 32000) {
        return true;
    } else {
        alert(`El número no puede ser mayor a 32000 ya que 
            el valor máximo del tipo de dato 
            Short es muy cercano a 32000`);
        return false
    }
}

// Boton para limpiar todos los espacios de texto en pantalla.
boton_limpiar.addEventListener('click', e => {
    numeroParaConvertir_id.value = '';
    shortResultado_id.value = '';
    intResultado_id.value = '';
    longResultado_id.value = '';
    floatResultado_id.value = '';
    doubleResultado_id.value = '';
});