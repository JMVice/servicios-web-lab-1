﻿// Variables relacionadas a la matriz de números aleatorios
const input1 = document.getElementById('input1');
const input2 = document.getElementById('input2');
const input3 = document.getElementById('input3');
const input4 = document.getElementById('input4');
const input5 = document.getElementById('input5');
const input6 = document.getElementById('input6');
const input7 = document.getElementById('input7');
const input8 = document.getElementById('input8');
const input9 = document.getElementById('input9');
const input10 = document.getElementById('input10');
const input11 = document.getElementById('input11');
const input12 = document.getElementById('input12');
const input13 = document.getElementById('input13');
const input14 = document.getElementById('input14');
const input15 = document.getElementById('input15');
const input16 = document.getElementById('input16');
const input17 = document.getElementById('input17');
const input18 = document.getElementById('input18');
const input19 = document.getElementById('input19');
const input20 = document.getElementById('input20');
const input21 = document.getElementById('input21');
const input22 = document.getElementById('input22');
const input23 = document.getElementById('input23');
const input24 = document.getElementById('input24');

// Variables relacionadas a los botones.
const boton_llenar = document.getElementById('boton_llenar');
const boton_limpiar = document.getElementById('boton_limpiar');
const boton_aplicar = document.getElementById('boton_aplicar');
const boton_limpiar2 = document.getElementById('boton_limpiar2');

// Variable relacionada al número limite.
const numeroLimite_id = document.getElementById('numeroLimite_id');

// Variables relacionadas a los textbox de parametros de los checkboxs
const valorPorFilaNumero = document.getElementById('valorPorFilaNumero');
const valorPorColumnaNumero = document.getElementById('valorPorColumnaNumero');

// Variables relacionadas a los radiobuttons.
const radioPorFilaNumero = document.getElementById('radioPorFilaNumero');
const radioPorColumnaNumero = document.getElementById('radioPorColumnaNumero');
const radioParaTodasLasFilas = document.getElementById('radioParaTodasLasFilas');
const radioParaTodasLasColumnas = document.getElementById('radioParaTodasLasColumnas');

// Booleanos para controlar los botones.
let botonLlenarArregloHabilitado = true;
let botonMostrarResultadosHabilitado = true;
let seHaCreadoUnArreglo = false;

// Boton que llena de números aleatorios la matriz.
boton_llenar.addEventListener('click', e => {

    // Se valida que el boton para rellenar el arreglo este habilitado.
    if (botonLlenarArregloHabilitado) {

        // Se valida que el numero limite es menor o igual a 100.
        if (parseInt(numeroLimite_id.value) <= 100) {

            // Se rellena la matriz con el número máximo dado.
            rellenarMatriz(generarArrayDeNumerosAleatorios(parseInt(numeroLimite_id.value)));

            // Se dehabilita el botón para crear un nuevo arreglo con números aleatorios.
            botonLlenarArregloHabilitado = false;

            // Se establece que se ha creado un nuevo arreglo para llevar control de las validaciones.
            seHaCreadoUnArreglo = true;
        } else {
            alert('El número limite máximo es 100');
        }
    } else {
        alert("Debe limpiar la matriz para volver a llenarla con otra nueva.");
    }
});

// Boton que aplica los resultados según los parametros del usuario.
boton_aplicar.addEventListener('click', e => {

    // Se valida si ya se ha creado un arreglo.
    if (seHaCreadoUnArreglo) {

        // Se valida si el boton de mostrar resultados está habilitado pues aun no se
        // a pedido por ningun resultado a mostrar.
        if (botonMostrarResultadosHabilitado) {

            // Se verifica si se ha selecionado el radiobutton de Por fila.
            if (radioPorFilaNumero.checked) {

                // Se optiene la fila seleccionada.
                let fila = parseInt(valorPorFilaNumero.value);

                // Se valida en un switch donde se va a hacer la sumatoria y muestra de resultados.
                switch (fila) {
                    case 1:
                        let valor_1 = parseInt(input1.value);
                        let valor_2 = parseInt(input2.value);
                        let valor_3 = parseInt(input3.value);
                        let valor_4 = parseInt(input4.value);

                        input5.value = valor_1 + valor_2 + valor_3 + valor_4;
                        botonMostrarResultadosHabilitado = false;
                        break;
                    case 2:
                        let valor_6 = parseInt(input6.value);
                        let valor_7 = parseInt(input7.value);
                        let valor_8 = parseInt(input8.value);
                        let valor_9 = parseInt(input9.value);

                        input10.value = valor_6 + valor_7 + valor_8 + valor_9;
                        botonMostrarResultadosHabilitado = false;
                        break;
                    case 3:
                        let valor_11 = parseInt(input11.value);
                        let valor_12 = parseInt(input12.value);
                        let valor_13 = parseInt(input13.value);
                        let valor_14 = parseInt(input14.value);

                        input15.value = valor_11 + valor_12 + valor_13 + valor_14;
                        botonMostrarResultadosHabilitado = false;
                        break;
                    case 4:
                        let valor_16 = parseInt(input16.value);
                        let valor_17 = parseInt(input17.value);
                        let valor_18 = parseInt(input18.value);
                        let valor_19 = parseInt(input19.value);

                        input20.value = valor_16 + valor_17 + valor_18 + valor_19;
                        botonMostrarResultadosHabilitado = false;
                        break;
                    default:
                        alert('Digite una fila valida.');
                }

            }

            // Se verifica si se ha selecionado el radiobutton de Por columna.
            if (radioPorColumnaNumero.checked) {

                // Se optiene la columna seleccionada.
                let columna = parseInt(valorPorColumnaNumero.value);

                // Se valida en un switch donde se va a hacer la sumatoria y muestra de resultados.
                switch (columna) {
                    case 1:
                        let valor_1 = parseInt(input1.value);
                        let valor_6 = parseInt(input6.value);
                        let valor_11 = parseInt(input11.value);
                        let valor_16 = parseInt(input16.value);

                        input21.value = valor_1 + valor_6 + valor_11 + valor_16;
                        botonMostrarResultadosHabilitado = false;
                        break;
                    case 2:
                        let valor_2 = parseInt(input2.value);
                        let valor_7 = parseInt(input7.value);
                        let valor_12 = parseInt(input12.value);
                        let valor_17 = parseInt(input17.value);

                        input22.value = valor_2 + valor_7 + valor_12 + valor_17;
                        botonMostrarResultadosHabilitado = false;
                        break;
                    case 3:
                        let valor_3 = parseInt(input3.value);
                        let valor_8 = parseInt(input8.value);
                        let valor_13 = parseInt(input13.value);
                        let valor_18 = parseInt(input18.value);

                        input23.value = valor_3 + valor_8 + valor_13 + valor_18;
                        botonMostrarResultadosHabilitado = false;
                        break;
                    case 4:
                        let valor_4 = parseInt(input4.value);
                        let valor_9 = parseInt(input9.value);
                        let valor_14 = parseInt(input14.value);
                        let valor_19 = parseInt(input19.value);

                        input24.value = valor_4 + valor_9 + valor_14 + valor_19;
                        botonMostrarResultadosHabilitado = false;
                        break;
                    default:
                        alert('Digite una columna valida.');
                }
            }

            // Se valida si se ha seleccionado para mostrar todos los resultados en para todas las filas.
            // Si es así, se hace la respectiva sumatoria y se muestran los resultados.
            if (radioParaTodasLasFilas.checked) {
                input5.value = parseInt(input1.value) + parseInt(input2.value) + parseInt(input3.value) + parseInt(input4.value);
                input10.value = parseInt(input6.value) + parseInt(input7.value) + parseInt(input8.value) + parseInt(input9.value);
                input15.value = parseInt(input11.value) + parseInt(input12.value) + parseInt(input13.value) + parseInt(input14.value);
                input20.value = parseInt(input16.value) + parseInt(input17.value) + parseInt(input18.value) + parseInt(input19.value);
                botonMostrarResultadosHabilitado = false;
            }

            // Se valida si se ha seleccionado para mostrar todos los resultados en para todas las columnas.
            // Si es así, se hace la respectiva sumatoria y se muestran los resultados.
            if (radioParaTodasLasColumnas.checked) {
                input21.value = parseInt(input1.value) + parseInt(input6.value) + parseInt(input11.value) + parseInt(input16.value);
                input22.value = parseInt(input2.value) + parseInt(input7.value) + parseInt(input12.value) + parseInt(input17.value);
                input23.value = parseInt(input3.value) + parseInt(input8.value) + parseInt(input13.value) + parseInt(input18.value);
                input24.value = parseInt(input4.value) + parseInt(input9.value) + parseInt(input14.value) + parseInt(input19.value);
                botonMostrarResultadosHabilitado = false;
            }

            // Si se llega aquí, se le muestra al usuario que debe seleccionar un radiobutton para mostrar resultados.
            if (!radioPorFilaNumero.checked &&
                !radioPorColumnaNumero.checked &&
                !radioParaTodasLasFilas.checked &&
                !radioParaTodasLasColumnas.checked) {
                alert("Debe seleccionar alguna de las opciones para aplicar resultados.");
            }
        } else {
            alert("Debe limpiar los resultados actuales para aplicar otros.");
        }
    } else {
        alert("Debe crear un arreglo antes de aplicar resultados.");
    }

});

// Boton que limpia toda la matriz.
boton_limpiar.addEventListener('click', e => {

    // Se llama a la funcion que limpia la matriz. Habilita además algunos botones y
    // validaciones.
    limpiarMatriz();
});

boton_limpiar2.addEventListener('click', e => {

    // Limpia las casillas de resultados. Habilita el botón para mostrar nuevos
    // resultados.
    limpiarEspaciosDeResultados();
});

// Funcion que retorna un array con 25 números aleatorios. Se generan dado
// un número máximo.
function generarArrayDeNumerosAleatorios(numeroMaximo) {

    let arreglo = [];

    for (let i = 0; i < 25; i++) {
        arreglo[i] = Math.floor(Math.random() * numeroMaximo + 1);
    }

    return arreglo;
}

// REllena toda la matriz con su equivalente dentro del arreglo creado.
function rellenarMatriz(arreglo) {
    input1.value = arreglo[0]
    input2.value = arreglo[1]
    input3.value = arreglo[2]
    input4.value = arreglo[3]
    input6.value = arreglo[5]
    input7.value = arreglo[6]
    input8.value = arreglo[7]
    input9.value = arreglo[8]
    input11.value = arreglo[10]
    input12.value = arreglo[11]
    input13.value = arreglo[12]
    input14.value = arreglo[13]
    input16.value = arreglo[15]
    input17.value = arreglo[16]
    input18.value = arreglo[17]
    input19.value = arreglo[18]
}

// Se llama a la funcion que limpia la matriz. Habilita además algunos botones y
// validaciones.
function limpiarMatriz() {

    // El boton para llenar el arreglo es habilitado nuevamente.
    botonLlenarArregloHabilitado = true;

    // El boolean que indica que hay un array creado cambia a falso
    seHaCreadoUnArreglo = false;
    input1.value = '';
    input2.value = '';
    input3.value = '';
    input4.value = '';
    input5.value = '';
    input6.value = '';
    input7.value = '';
    input8.value = '';
    input9.value = '';
    input10.value = '';
    input11.value = '';
    input12.value = '';
    input13.value = '';
    input14.value = '';
    input15.value = '';
    input16.value = '';
    input17.value = '';
    input18.value = '';
    input19.value = '';
    input20.value = '';
    input21.value = '';
    input22.value = '';
    input23.value = '';
    input24.value = '';
}

// Limpia las casillas de resultados. Habilita el botón para mostrar nuevos
// resultados.
function limpiarEspaciosDeResultados() {
    botonMostrarResultadosHabilitado = true;
    input21.value = '';
    input22.value = '';
    input23.value = '';
    input24.value = '';
    input20.value = '';
    input15.value = '';
    input10.value = '';
    input5.value = '';
}
