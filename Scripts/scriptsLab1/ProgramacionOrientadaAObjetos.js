﻿// Variables correspondientes a las cajas de texto para crear empleados.
const nombreEmpleado_id = document.getElementById("nombreEmpleado_id");
const apellidoEmpleado_id = document.getElementById("apellidoEmpleado_id");
const dniEmpleado_id = document.getElementById("dniEmpleado_id");
const empleadoCodigoDepartamento_id = document.getElementById("empleadoCodigoDepartamento_id");

// Variables correspondientes a las cajas de texto para crear departamentos.
const codigoDepartamento_id = document.getElementById("codigoDepartamento_id");
const nombreDepartamento_id = document.getElementById("nombreDepartamento_id");
const presupuestoDepartamento_id = document.getElementById("presupuestoDepartamento_id");

// Variables correspondientes a los botones para crear empleados y departamentos.
const boton_crearNuevoEmpleado = document.getElementById("boton_crearNuevoEmpleado");
const boton_crearNuevoDepartamento = document.getElementById("boton_crearNuevoDepartamento");

// Variables correspondientes a las tablas de departamentos y empleados.
const tablaEmpleados_id = document.getElementById("tablaEmpleados_id");
const tablaDepartamentos_id = document.getElementById("tablaDepartamentos_id");

// Arreglos para los departamentos y empleados.
let empleados = [];
let departamentos = [];

// Funcion de crear un nuevo empleado.
boton_crearNuevoEmpleado.addEventListener('click', e => {

    // Se valida que todas las casillas para crear un nuevo empleado esten llenas.
    if (estanTodasLasCasillasDeEmpleadoLlenas()) {
        // Debe existir al menos un departamento para guardar un empleado.
        if (departamentos.length > 0) {

            // El DNI del empleado debe ser unico.
            if (esElDNIunico(dniEmpleado_id.value)) {

                // El código de departamento del empleado debe existir con un codigo
                // de departamento real.
                if (existeElDepartamento(empleadoCodigoDepartamento_id.value)) {

                    // Funcion que introduce los parametros del empleado en la tabla
                    // en el HTML.
                    introducirEmpleadoEnLaTabla();

                    // Crea un objeto de tipo Empleado y lo guarda dentro del arreglo
                    // de empleados.
                    crearObjectoEmpleadoYGuardarEnArreglo();

                    // Funcion que limpia las cajas de texto de los empleados.
                    limpiarCajasDeTextoEmpleados();
                } else {
                    alert(`El código de departamento no pertenece 
                    a ningun departamento registrado`);
                }
            } else {
                alert("El DNI debe ser unico.");
            }
        } else {
            alert("Tiene que crear primero un departamento.");
        }
    } else {
        alert('Debe llenar todas las casillas para crear un nuevo empleado.');
    }
});

// Funcion de crear un nuevo departamento.
boton_crearNuevoDepartamento.addEventListener('click', e => {

    // Se valida que todas las casillas para crear un nuevo departamento esten llenas.
    if (estanTodasLasCasillasDeDepartamentoLlenas()) {
        if (esElCodigounico(codigoDepartamento_id.value)) {

            // Se introduce el nuevo departamento en la table del HTML.
            introducirDepartamentoEnLaTabla();

            // Se crea un objeto con base al arreglo y se guarda dentro del arreglo.
            crearObjectoDepartamentoYGuardarEnArreglo();

            // Se limpian las cajas de texto del departamento.
            limpiarCajasDeTextoDepartamento();
        } else {
            alert('El departamento debe tener un codigo unico');
        }
    } else {
        alert('Debe llenar todas las casillas para crear un nuevo departamento.');
    }

});

// Funcion que valida si el DNI introducido es unico.
function esElDNIunico(dni) {
    if (empleados.length == 0) {
        return true;
    } else {
        for (let i = 0; i < empleados.length; i++) {
            if (dni == empleados[i].dni) {
                return false;
            }
        }
        return true;
    }
}

// Funcion que valida si el codigo de departamento introducido es unico.
function esElCodigounico(codigo) {
    if (departamentos.length == 0) {
        return true;
    } else {
        for (let i = 0; i < departamentos.length; i++) {
            if (codigo == departamentos[i].codigo) {
                return false;
            }
        }
        return true;
    }
}

// Funcion que valida si el codigo de departamento del empleado
// corresponde al codigo de un departamento guardado.
function existeElDepartamento(codigoDepartamento) {
    for (let i = 0; i < departamentos.length; i++) {
        if (codigoDepartamento == departamentos[i].codigo) {
            return true;
        }
    }
    return false;
}

// Agrega los parametros dados por el usuario del departamento y los agrega
// en su tabla correspondiente en el HTML.
function introducirDepartamentoEnLaTabla() {
    var row = tablaDepartamentos_id.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    cell1.innerHTML = codigoDepartamento_id.value;
    cell2.innerHTML = nombreDepartamento_id.value;
    cell3.innerHTML = presupuestoDepartamento_id.value;
}

// Agrega los parametros dados por el usuario del empleado y los agrega
// en su tabla correspondiente en el HTML.
function introducirEmpleadoEnLaTabla() {
    var row = tablaEmpleados_id.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    cell1.innerHTML = nombreEmpleado_id.value;
    cell2.innerHTML = apellidoEmpleado_id.value;
    cell3.innerHTML = dniEmpleado_id.value;
    cell4.innerHTML = empleadoCodigoDepartamento_id.value;
}

// Crea un objeto de tipo Empleado y lo guarda dentro de su respectivo
// arreglo.
function crearObjectoEmpleadoYGuardarEnArreglo() {
    empleado = new Empleado(nombreEmpleado_id.value
        , apellidoEmpleado_id.value
        , empleadoCodigoDepartamento_id.value
        , dniEmpleado_id.value);

    empleados.push(empleado);
    console.log(empleados);
}

// Crea un objeto de tipo Departamento y lo guarda dentro de su respectivo
// arreglo.
function crearObjectoDepartamentoYGuardarEnArreglo() {
    departamento = new Departamento(codigoDepartamento_id.value
        , nombreDepartamento_id.value
        , presupuestoDepartamento_id.value);

    departamentos.push(departamento);
}

function limpiarCajasDeTextoEmpleados() {
    nombreEmpleado_id.value = '';
    apellidoEmpleado_id.value = '';
    dniEmpleado_id.value = '';
    empleadoCodigoDepartamento_id.value = '';
}

function limpiarCajasDeTextoDepartamento() {
    codigoDepartamento_id.value = '';
    nombreDepartamento_id.value = '';
    presupuestoDepartamento_id.value = '';
}

// Funcion que valida si todas las casillas están llenas para crear un nuevo
// empleado.
function estanTodasLasCasillasDeEmpleadoLlenas() {
    if (nombreEmpleado_id.value != '' &&
        apellidoEmpleado_id.value != '' &&
        dniEmpleado_id.value != '' &&
        empleadoCodigoDepartamento_id.value != '') {
        return true;
    } else {
        return false;
    }
}

// Funcion que valida si todas las casillas están llenas para crear un nuevo
// departamento.
function estanTodasLasCasillasDeDepartamentoLlenas() {
    if (codigoDepartamento_id.value != '' &&
        nombreDepartamento_id.value != '' &&
        presupuestoDepartamento_id.value != '') {
        return true;
    } else {
        return false;
    }
}