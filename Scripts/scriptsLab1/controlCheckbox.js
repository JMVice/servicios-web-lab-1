﻿// El arreglo generado.
var arreglo = [];

// Variables que hacen referencia a los elementos de cada de texto de tamaño del arreglo y limite del arreglo.
const tamanioArreglo_id = document.getElementById("tamanioArreglo_id");
const limiteDelRandom_id = document.getElementById("limiteDelRandom_id");

// Variables que hacen referencia a los checkbox.
const removerNumerosPares_id = document.getElementById("removerNumerosPares_id");
const removerNumerosMenoresA10_id = document.getElementById("removerNumerosMenoresA10_id");
const removerNumerosMayoresA50 = document.getElementById("removerNumerosMayoresA50");

// Variables que hacen referencia a los botones.
const boton_llenarArreglo = document.getElementById("boton_llenarArreglo");
const boton_verArreglo = document.getElementById("boton_verArreglo");
const boton_limpiar = document.getElementById("boton_limpiar");

// Panel de texto donde se muestra el arreglo.
const panelTextoArreglo_id = document.getElementById("panelTextoArreglo_id");

// Acción del botón para llenar el arreglo.
boton_llenarArreglo.addEventListener('click', e => {

    // Se recogen los parametros para construir el arreglo.
    let tamanioArreglo_valor = parseInt(tamanioArreglo_id.value);
    let limiteDelRandom_valor = parseInt(limiteDelRandom_id.value);

    // Se hace una comprobación si el limite máximo del arreglo es de 100
    if (limiteDelRandom_valor <= 100) {

        // Se llena el arreglo con los numeros segun los parametros establecidos.
        for (let i = 0; i < tamanioArreglo_valor; i++) {

            //Se usa la funcion para generar números aleatorios segun el rango máximo.
            arreglo.push(generarNumeroRandom(limiteDelRandom_valor));
        }
        alert('El arreglo ha sido llenado.');
        console.log(arreglo)

        // Deshabilita la escritura dentro de los paneles del array.
        panelesDeTextoSoloDeLectura(true);
    } else {
        alert('No puede generar un arreglo con números mayores a 100.');
    }
});

// Acción del botón para ver el arreglo en caja de teto Contenido del Arreglo.s
boton_verArreglo.addEventListener('click', e => {

    // Se crea un arreglo editado para modificarse a los controles que el usuario
    // establesca.
    let arregloEditado = arreglo;

    if (removerNumerosPares_id.checked) {

        // Se crea un arreglo temporal para filtrar los numeros pares.
        let arregloTemporal = [];
        for (let i = 0; i < arregloEditado.length; i++) {

            // Si el numero es divisible entre dos y su reciduo es 0, significa que
            // es número par.
            if (arregloEditado[i] % 2 !== 0) {

                // Por lo tanto se agrega al arreglo temporal.
                arregloTemporal.push(arregloEditado[i]);
            }
        }

        // Finalmente se actualiza el arreglo Editado con el arreglo temporal sin numeros pares.
        arregloEditado = arregloTemporal;
    }

    if (removerNumerosMenoresA10_id.checked) {

        // Se crea un arreglo temporal para filtrar los numeros menores a 10.
        let arregloTemporal = [];

        for (let i = 0; i < arregloEditado.length; i++) {

            // Si el numero es menor o igual a 10, es escluido.
            if (arregloEditado[i] >= 10) {
                arregloTemporal.push(arregloEditado[i]);
            }
        }
        // Finalmente se actualiza el arreglo Editado con el arreglo temporal sin numeros
        // menores a 10.
        arregloEditado = arregloTemporal;
    }

    if (removerNumerosMayoresA50.checked) {

        // Se crea un arreglo temporal para filtrar los numeros mayores a 50.
        let arregloTemporal = [];
        for (let i = 0; i < arregloEditado.length; i++) {

            // Si el numero es mayor o igual a 50, es excluido.
            if (arregloEditado[i] <= 50) {
                arregloTemporal.push(arregloEditado[i]);
            }
        }

        // Finalmente se actualiza el arreglo Editado con el arreglo temporal sin numeros
        // mayores a 50.
        arregloEditado = arregloTemporal;
    }

    // Se crea una cadena de texto que contiene todos los numeros dentro del arreglo editado.
    let cadenaDeTextoDelArreglo = "";
    for (let i = 0; i < arregloEditado.length; i++) {
        cadenaDeTextoDelArreglo += arregloEditado[i] + ',';
    }

    // Se establece la cadena de texto en el panel de texto que muestra el arreglo.
    panelTextoArreglo_id.value = cadenaDeTextoDelArreglo;
});

// Acción del botón para limpiar todo el formulario.
boton_limpiar.addEventListener('click', e => {
    // Limpia el contenido del arreglo.
    arreglo = new Array();

    // Limpia las cajas de texto.
    tamanioArreglo_id.value = '';
    limiteDelRandom_id.value = '';
    panelTextoArreglo_id.value = '';

    // Quita los checks de los checkbox
    removerNumerosPares_id.checked = false;
    removerNumerosMenoresA10_id.checked = false;
    removerNumerosMayoresA50.checked = false;

    // Habilita la escritura dentro de los paneles del array.
    panelesDeTextoSoloDeLectura(false);
});

// Función que genera números alteatorios según un numero máximo dado.
function generarNumeroRandom(maximoNumeroAGenerar) {
    return Math.floor(Math.random() * maximoNumeroAGenerar + 1);
}

// Retorna true si el numero dado es par, sino lo es, retorna false.
function esNumeroPar(numero) {
    if (numero % 2 === 0) {
        return true;
    } else {
        return false;
    }
}

// Funcion para deshabilitar o habilitar el poder escribir dentro de los
// bloques de texto para Tamaño del Arreglo y Limite del Random. Si se le
// pasa true solo se podra leer el contenido. Si se pasa false, se podra leer
// y escribir en ellos.
function panelesDeTextoSoloDeLectura(boolean) {
    tamanioArreglo_id.readOnly = boolean
    limiteDelRandom_id.readOnly = boolean
}