﻿// Variables correspondientes a las cajas de texto de las hileras.
const hilera1_id = document.getElementById("hilera1_id");
const hilera2_id = document.getElementById("hilera2_id");

// Variables correspondientes a las cajas de texto de resultados.
const hileraMasLarga_id = document.getElementById("hileraMasLarga_id");
const hileraConMasVocales_id = document.getElementById("hileraConMasVocales_id");
const hileraConMasConsonantes_id = document.getElementById("hileraConMasConsonantes_id");
const hileraConMasNoAlfanumericos_id = document.getElementById("hileraConMasNoAlfanumericos_id");

// Variables correspondientes a los botones.
const boton_ver = document.getElementById("boton_ver");
const boton_limpiar = document.getElementById("boton_limpiar");

// Bloque de código de el boton ver.
boton_ver.addEventListener('click', e => {

    let hilera_1 = hilera1_id.value;
    let hilera_2 = hilera2_id.value;

    establecerHileraMasLarga(hilera_1, hilera_2);

    establecerHileraConMasVocales(hilera_1, hilera_2);

    establecerHileraConMasConsonantes(hilera_1, hilera_2);

    establecerHileraConMasNoAlfanumericos(hilera_1, hilera_2);

});

// Boton que limpia todas las casillas de texto.
boton_limpiar.addEventListener('click', e => {
    hilera1_id.value = '';
    hilera2_id.value = '';
    hileraMasLarga_id.value = '';
    hileraConMasVocales_id.value = '';
    hileraConMasConsonantes_id.value = '';
    hileraConMasNoAlfanumericos_id.value = '';
});

function establecerHileraMasLarga(hilera_1, hilera_2) {
    let longitudHilera1 = hilera_1.length;
    let longitudHilera2 = hilera_2.length;

    if (longitudHilera1 > longitudHilera2) {
        hileraMasLarga_id.value = hilera_1
    } else {
        hileraMasLarga_id.value = hilera_2
    }
}

function establecerHileraConMasVocales(hilera_1, hilera_2) {
    let vocales = ['a', 'e', 'i', 'o', 'u'];

    let cantidadVocales_hilera1 = 0;
    let cantidadVocales_hilera2 = 0;

    for (let i = 0; i < hilera_1.length; i++) {
        for (let j = 0; j < vocales.length; j++) {
            if (hilera_1[i].toLowerCase() == vocales[j]) {
                cantidadVocales_hilera1++;
                break;
            }
        }
    }

    for (let i = 0; i < hilera_2.length; i++) {
        for (let j = 0; j < vocales.length; j++) {
            if (hilera_2[i].toLowerCase() == vocales[j]) {
                cantidadVocales_hilera2++;
                break;
            }
        }
    }

    if (cantidadVocales_hilera1 > cantidadVocales_hilera2) {
        hileraConMasVocales_id.value = hilera_1;
    } else {
        hileraConMasVocales_id.value = hilera_2;
    }

}

function establecerHileraConMasConsonantes(hilera_1, hilera_2) {
    let consonantes = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'y', 'z'];

    let cantidadConsonantes_hilera1 = 0;
    let cantidadConsonantes_hilera2 = 0;

    for (let i = 0; i < hilera_1.length; i++) {
        for (let j = 0; j < consonantes.length; j++) {
            if (hilera_1[i].toLowerCase() == consonantes[j]) {
                cantidadConsonantes_hilera1++;
                break;
            }
        }
    }

    for (let i = 0; i < hilera_2.length; i++) {
        for (let j = 0; j < consonantes.length; j++) {
            if (hilera_2[i].toLowerCase() == consonantes[j]) {
                cantidadConsonantes_hilera2++;
                break;
            }
        }
    }

    if (cantidadConsonantes_hilera1 > cantidadConsonantes_hilera2) {
        hileraConMasConsonantes_id.value = hilera_1;
    } else {
        hileraConMasConsonantes_id.value = hilera_2;
    }
}

function establecerHileraMasLarga(hilera_1, hilera_2) {
    let longitudHilera1 = hilera_1.length;
    let longitudHilera2 = hilera_2.length;

    if (longitudHilera1 > longitudHilera2) {
        hileraMasLarga_id.value = hilera_1
    } else {
        hileraMasLarga_id.value = hilera_2
    }
}

function establecerHileraConMasNoAlfanumericos(hilera_1, hilera_2) {
    let noAlphanumericos = [`!`, `@`, `#`, `&`, `(`, `)`, `–`, `[`, `{`, `}`, `]`, `:`, `;`, `'`, `?`, `/`, `*`, `~`, `$`, `^`, `+`, `=`, `<`, `>`, `,`];

    let cantidad_hilera1 = 0;
    let cantidad_hilera2 = 0;

    for (let i = 0; i < hilera_1.length; i++) {
        for (let j = 0; j < noAlphanumericos.length; j++) {
            if (hilera_1[i].toLowerCase() == noAlphanumericos[j]) {
                cantidad_hilera1++;
                break;
            }
        }
    }

    for (let i = 0; i < hilera_2.length; i++) {
        for (let j = 0; j < noAlphanumericos.length; j++) {
            if (hilera_2[i].toLowerCase() == noAlphanumericos[j]) {
                cantidad_hilera2++;
                break;
            }
        }
    }

    if (cantidad_hilera1 > cantidad_hilera2) {
        hileraConMasNoAlfanumericos_id.value = hilera_1;
    } else {
        hileraConMasNoAlfanumericos_id.value = hilera_2;
    }
}