﻿// Instancia de variable constante para los botones: calcular y limpiar.
const boton_calcular = document.getElementById('boton_calcular');
const boton_limpiar = document.getElementById('boton_limpiar');

// Instancia de variables para los valores introducidos de salario por horas,
// dias trabajados y porcentaje de deducciones. Se puede obtener sus valores
// con: elemento.value .
let salarioPorHoras_id = document.getElementById('salarioPorHoras_id');
let diasTrabajados_id = document.getElementById('diasTrabajados_id');
let deducciones_id = document.getElementById('deducciones_id');

// Instancia de variables de las cajas de texto de resulados.
let salarioBrutoResultado_id = document.getElementById('salarioBrutoResultado_id');
let deduccionesResultado_id = document.getElementById('deduccionesResultado_id');
let salarioNetoResultado_id = document.getElementById('salarioNetoResultado_id');

// Codigo a ejecutar cuando se de click al boton calcular.
boton_calcular.addEventListener('click', e => {

    // Condicional para saber si todos los valores se han ingresado.
    if (salarioPorHoras_id.value && diasTrabajados_id.value && deducciones_id.value) {

        // Condicional para saber si el porcentaje de deducciones es un número entero.
        if (Number.isInteger(parseFloat(deducciones_id.value))) {

            // Elaboración de los calculos y guardado en variables.
            let salarioBruto_resultado = (parseInt(salarioPorHoras_id.value) * 7) * parseInt(diasTrabajados_id.value);
            let deducciones_resultado = (salarioBruto_resultado / 100) * parseInt(deducciones_id.value);
            let salarioNeto_resultado = salarioBruto_resultado - deducciones_resultado;

            // Asignación de resultados en las cajas de texto correspondiente.
            salarioBrutoResultado_id.value = salarioBruto_resultado;
            deduccionesResultado_id.value = deducciones_resultado;
            salarioNetoResultado_id.value = salarioNeto_resultado;

        } else {
            alert('El porcentaje de deducciones debe ser un número entero.');
        }
    } else {
        alert("Debe llenar las cajas de texto Salario por horas, días trabajados y % deducciones.");
    }
});

// Codigo a ejecutar cuando se de click al boton limpiar.
// Su proposito es limpiar todas las cajas de texto.
boton_limpiar.addEventListener('click', e => {
    salarioPorHoras_id.value = '';
    diasTrabajados_id.value = '';
    deducciones_id.value = '';
    salarioBrutoResultado_id.value = '';
    deduccionesResultado_id.value = '';
    salarioNetoResultado_id.value = '';
});