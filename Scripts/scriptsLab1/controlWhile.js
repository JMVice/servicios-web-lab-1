﻿// Variables que hacen referencia a las cajas de texto de parametros editables.
// Tipo de cliente y subtotal
const tipoDeCliente_id = document.getElementById('tipoDeCliente_id');
const subtotal_id = document.getElementById('subtotal_id');

// Variables que hacen referencia a las cajas de texto de resultados.
// Porcentaje de descuento, monto de descuento y el total.
const porcentajeDescuento_id = document.getElementById('porcentajeDescuento_id');
const montoDescuento_id = document.getElementById('montoDescuento_id');
const total_id = document.getElementById('total_id');

// Variable que hace referencia al botón de calcular.
const boton_calcular = document.getElementById('boton_calcular');

// Lo que hará el botón cuando se le de click.
boton_calcular.addEventListener('click', e => {

    // Relación descuento y cliente:
    //A: 25% de descuento.
    //B: 20% de descuento.
    //C: 15% de descuento.
    //D: 10% de descuento.
    //E: 5% de descuento.

    let tipoCliente_valorIntroducido = tipoDeCliente_id.value.toUpperCase();
    let subtotal_valorIntroducido = parseInt(subtotal_id.value);

    // El contador se utilizara para hacer un control en el while.
    let contador = 0;

    // Arreglo que contiene los tipos de clientes con descuento.
    let arregloClientes = ['A', 'B', 'C', 'D', 'E'];

    // Boolean para saber si es un cliente con descuento.
    let esClienteConDescuento = false;

    // Se evalua con el bucle while si el cliente tiene descuento.
    while (contador < arregloClientes.length) {
        if (tipoCliente_valorIntroducido == arregloClientes[contador]) {

            // Se establece que el cliente SÍ tiene descuento.
            esClienteConDescuento = true;
            break;
        }
        contador++;
    }

    if (esClienteConDescuento) {

        let porcentajeDeDescuento_valor = 0;
        let montoDeDescuento_valor = 0;

        switch (tipoCliente_valorIntroducido) {
            case "A":
                // Establece el descuento en 25% .
                porcentajeDeDescuento_valor = 25;
                break;
            case "B":
                // Establece el descuento en 20% .
                porcentajeDeDescuento_valor = 20;
                break;
            case "C":
                // Establece el descuento en 15% .
                porcentajeDeDescuento_valor = 15;
                break;
            case "D":
                // Establece el descuento en 10% .
                porcentajeDeDescuento_valor = 10;
                break;
            case "E":
                // Establece el descuento en 5% .
                porcentajeDeDescuento_valor = 5;
                break;
            default:
        }

        // Se calcula y establece el monto de descuento.
        montoDeDescuento_valor = ((porcentajeDeDescuento_valor / 100) * subtotal_valorIntroducido);

        // Establece el porcentaje de descuento en la caja de texto de porcentaje de descuento.
        porcentajeDescuento_id.value = porcentajeDeDescuento_valor + "%";

        // Establece el monto de descuento en caja de texto monto de descuento
        montoDescuento_id.value = montoDeDescuento_valor

        // Establece el total con descuento en la caja de texto de total.
        total_id.value = subtotal_valorIntroducido - montoDeDescuento_valor;

    } else {
        // No posee descuento por lo que el total es igual al subtotal.
        total_id.value = subtotal_valorIntroducido;
        limpiarCajasDeTextoDeDescuento();
    }

});

// Si se introduce un cliente sin descuento, se limpian las cajas de texto que corresponden a
// el valor de descuento
function limpiarCajasDeTextoDeDescuento() {
    porcentajeDescuento_id.value = '';
    montoDescuento_id.value = '';
}